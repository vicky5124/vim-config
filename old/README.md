# How to install the things :D

Both the `init.vim` and `coc-settings.json` file go on `~/.config/nvim/`

Inistall initial dependencies: `pacman -S python2-pip python-pip npm ruby xclip`

Install libraries required for some of the pluggins:

- `pip2 install neovim pynvim -U --user`
- `pip3 install neovim pynvim -U --user`
- `sudo npm install -g neovim coc-clangd`
- `gem install neovim`
- `echo 'PATH="$HOME/.gem/ruby/2.7.0/bin:$PATH"' >> ~/.profile`

Install [Vim Plug](https://github.com/junegunn/vim-plug)\

```bash
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

Install the plugins with `:PlugInstall` and restart neovim.

Install COC stuff: `:CocInstall coc-json coc-tsserver coc-java coc-rust-analyzer coc-python coc-discord-rpc`
