# NeoVim configuration files

System dependencies include `python3` with `pip`, `node.js` with `npm` and `rust-lang` with `cargo`.
In Arch Linux, all dependencies can be installed with:

```bash
sudo pacman -S neovim python-pip python-pynvim node npm rustup
```

Start by installing packer:

```bash
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

Then copy all the files in the `./current` folder onto `$HOME/.config/nvim`.

Open neovim and run `:PackerSync` to install all the plugins.
Restart neovim afterwards.

Configure COQ by running `:COQdeps` after restart.

To install all the language servers configured, run:

```vim
:MasonInstall angular-language-server angularls clangd codelldb css-lsp cssls firefox-debug-adapter jdtls json-lsp jsonls prettier pyright ruff ruff-lsp ruff_lsp rust-analyzer rust_analyzer typescript-language-server ts_ls vacuum vim-language-server vimls zls
```

That's it!
