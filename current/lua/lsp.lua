-- Mason Setup
require("mason").setup({
    ui = {
        icons = {
            package_installed = "",
            package_pending = "",
            package_uninstalled = "",
        },
    }
})

require("mason-lspconfig").setup()

-- Setup language servers.
local lspconfig = require("lspconfig")
local coq = require("coq")

-- JSON / OpenApi (JSON + YAML)
vim.filetype.add {
    pattern = {
        ["openapi.*%.ya?ml"] = "yaml.openapi",
        ["openapi.*%.json"] = "json.openapi",
    },
}
lspconfig.vacuum.setup {coq.lsp_ensure_capabilities{}}

lspconfig.jsonls.setup {coq.lsp_ensure_capabilities{}}

-- Vim script
lspconfig.vimls.setup {}

-- Python
lspconfig.ruff.setup {coq.lsp_ensure_capabilities{}}
lspconfig.pyright.setup {coq.lsp_ensure_capabilities{}}

-- TS / JS
lspconfig.angularls.setup{coq.lsp_ensure_capabilities{}}

local ts_tools_api = require("typescript-tools.api")
require("typescript-tools").setup {
    handlers = {
        ["textDocument/publishDiagnostics"] = ts_tools_api.filter_diagnostics({ 80001 }),
    }
}

-- JAVA
lspconfig.jdtls.setup {coq.lsp_ensure_capabilities{}}

-- Zig
lspconfig.zls.setup {coq.lsp_ensure_capabilities{}}

-- C / C++
lspconfig.clangd.setup {coq.lsp_ensure_capabilities{}}

-- CSS / SCSS / LESS
lspconfig.cssls.setup {coq.lsp_ensure_capabilities{}}

-- Rust
lspconfig.rust_analyzer.setup {
    coq.lsp_ensure_capabilities{
        -- Server-specific settings. See `:help lspconfig-setup`
        settings = {
            ["rust-analyzer"] = {},
        },
    }
}


-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set("n", "<space>l", vim.diagnostic.open_float)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next)
vim.keymap.set("n", "<space>e", vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd("LspAttach", {
    group = vim.api.nvim_create_augroup("UserLspConfig", {}),
    callback = function(ev)
        -- Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

        -- Buffer local mappings.
        -- See `:help vim.lsp.*` for documentation on any of the below functions
        local opts = { buffer = ev.buf }
        vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
        vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
        vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts)
        vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, opts)
        vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, opts)
        vim.keymap.set("n", "<space>wl", function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, opts)
        vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, opts)
        vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)
        vim.keymap.set({ "n", "v" }, "<space>ca", vim.lsp.buf.code_action, opts)
        vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
        vim.keymap.set("n", "<space>f", function()
            vim.lsp.buf.format { async = true }
        end, opts)
    end,
})

-- Disable stupid Rust LSP error message
for _, method in ipairs({ 'textDocument/diagnostic', 'workspace/diagnostic' }) do
    local default_diagnostic_handler = vim.lsp.handlers[method]
    vim.lsp.handlers[method] = function(err, result, context, config)
        if err ~= nil and err.code == -32802 then
            return
        end
        return default_diagnostic_handler(err, result, context, config)
    end
end
