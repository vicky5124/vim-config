return require("packer").startup(function(use)
    -- Packer itself
    use {"wbthomason/packer.nvim"}

    -- Code snippets, templates and autocomplete { }
    use {"drmingdrmer/xptemplate"}

    -- Asynchronous build and test dispatcher
    use {"tpope/vim-dispatch", opt = true, cmd = {"Dispatch", "Make", "Focus", "Start"}}

    -- Navigate and highlight matching words
    use {
        "andymass/vim-matchup",
        setup = function()
            -- may set any options here
            vim.g.matchup_matchparen_offscreen = { method = "popup" }
        end
    }

    -- Asynchronous linter and syntax checker
    use {
        "w0rp/ale",
        cmd = "ALEEnable",
        config = "vim.cmd[[ALEEnable]]"
    }

    -- Real-time markdown previewer
    use {"iamcco/markdown-preview.nvim", run = "cd app && yarn install", cmd = "MarkdownPreview"}

    -- Improved syntax hightlighting
    use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }

    -- Status line
    use {
        "nvim-lualine/lualine.nvim",
        requires = {"kyazdani42/nvim-web-devicons"}
    }

    -- Git integration
    use {
        "lewis6991/gitsigns.nvim", requires = { "nvim-lua/plenary.nvim" },
        config = function() require("gitsigns").setup() end
    }

    -- Colour-scheme plugin
    use {"tjdevries/colorbuddy.vim"}

    -- Theme
    use {"catppuccin/nvim", as = "catppuccin"}

    -- Vim File Manager
    use {
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v3.x",
        requires = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
            "MunifTanjim/nui.nvim",
        }
    }

    -- Easily align text and symbols.
    use {"junegunn/vim-easy-align"}

    -- Diagnostic references
    use {"folke/trouble.nvim"}
    use {"nvim-telescope/telescope.nvim"}

    -- Highlight the yanked selection
    use {"machakann/vim-highlightedyank"}

    -- Source code explorer
    use {"liuchengxu/vista.vim"}

    -- Toggle booleans
    use {"sagarrakshe/toggle-bool"}

    -- Show the colour of various colour patterns in code.
    use {"lilydjwg/colorizer"}

    -- Highlight all instances of words on cursor
    use {"RRethy/vim-illuminate"}

    -- Add autoformatting to the Code
    use {"Chiel92/vim-autoformat"}

    -- Edit and insert to surroundings
    use {"tpope/vim-surround"}

    -- Markdown utils
    use {"preservim/vim-markdown"}

    -- Language Server configs
    use {"neovim/nvim-lspconfig"}

    -- Autocomplete
    use {
        "ms-jpq/coq_nvim",
        branch = "coq",
        requires = {
            {"ms-jpq/coq.thirdparty", branch = "3p" },
            {"ms-jpq/coq.artifacts", branch = "artifacts" },
        }
    }

    -- Rust crate manager
    use {
        "saecki/crates.nvim",
        branch = "main",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            require("crates").setup()
        end,
    }

    -- Additional rust LSP features
    use {"simrat39/rust-tools.nvim"}
    use {"nvim-lua/plenary.nvim"}
    use {"mfussenegger/nvim-dap"}

    -- Manage language server installation
    use {"williamboman/mason.nvim"}
    use {"williamboman/mason-lspconfig.nvim"}

    -- vim debugger
    use {"puremourning/vimspector"}

    -- LSP progress bar for lualine
    use {"arkav/lualine-lsp-progress"}

    -- Discord RPC
    use {"andweeb/presence.nvim"}

    -- Zig Lang
    use {"ziglang/zig.vim"}

    -- TS / JS
    use {
        "pmizio/typescript-tools.nvim",
        requires = {
            "nvim-lua/plenary.nvim",
            "neovim/nvim-lspconfig"
        },
        config = function ()
            require("typescript-tools").setup {}
        end,
    }
end)
